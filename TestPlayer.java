import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import oxgame.*;

public class TestPlayer {
    
    public TestPlayer() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testNameX(){
        Player p1 = new Player('x');
        assertEquals('x',p1.getName());
    }
    
    @Test
    public void testNameO(){
        Player p1 = new Player('o');
        assertEquals('o',p1.getName());
    }
}
